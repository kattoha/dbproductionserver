package com.dbprod.overview.server.utils;

import com.dbprod.overview.server.dto.ApplicationGroupInfoDTO;
import com.dbprod.overview.server.models.ApplicationGroup;
import lombok.NonNull;

/**
 * GroupUtils POJO
 *
 * @author kapitoha
 */
public class GroupUtils {
    /**
     * Update existed entity with data from dto
     * @param entity {@link ApplicationGroup}
     * @param dto {@link ApplicationGroupInfoDTO}
     */
    public static void resolveGroup(@NonNull ApplicationGroup entity, @NonNull ApplicationGroupInfoDTO dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setHideName(dto.isHideName());
    }
}

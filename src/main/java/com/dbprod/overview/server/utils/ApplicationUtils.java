package com.dbprod.overview.server.utils;

import com.dbprod.overview.server.dto.ApplicationDTO;
import com.dbprod.overview.server.models.GalleryApplication;
import com.dbprod.overview.server.repositories.ApplicationGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ApplicationUtils POJO
 *
 * @author kapitoha
 */
@Component
public class ApplicationUtils {
    private static ApplicationGroupRepository groupRepository;

    @Autowired
    protected void init(ApplicationGroupRepository groupRepository) {
        ApplicationUtils.groupRepository = groupRepository;
    }

    /**
     * Fill application data from DTO
     *
     * @param application {@link GalleryApplication}
     * @param dto         {@link ApplicationDTO}
     */
    public static void resolveApplication(GalleryApplication application, ApplicationDTO dto) {
        application.setId(dto.getId());
        application.setName(dto.getName());
        application.setDescription(dto.getDescription());
        application.setImage(dto.getImage());
        application.setLink(dto.getLink());
        application.setGroup(groupRepository.getOne(dto.getGroup()));
        application.setOrderIndex(application.getGroup().getId() == dto.getGroup()? dto.getOrderIndex() : 0);
    }
}

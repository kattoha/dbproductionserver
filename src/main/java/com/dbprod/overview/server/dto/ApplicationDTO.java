package com.dbprod.overview.server.dto;

import com.dbprod.overview.server.models.GalleryApplication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * ApplicationDTO POJO
 *
 * @author kapitoha
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationDTO {
    private Integer id;
    private String name;
    private String description;
    private String image;
    private String link;
    private int group;
    private int orderIndex;

    public ApplicationDTO(@NonNull GalleryApplication application) {
        this.id = application.getId();
        this.name = application.getName();
        this.description = application.getDescription();
        this.link = application.getLink();
        this.image = application.getImage();
        this.group = application.getGroup().getId();
        this.orderIndex = application.getOrderIndex();
    }
}

package com.dbprod.overview.server.dto;

import com.dbprod.overview.server.models.ApplicationGroup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ApplicationGroupInfoDTO POJO
 *
 * @author kapitoha
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicationGroupInfoDTO {
    private Integer id;
    private String name;
    private String description;
    private boolean hideName;

    /**
     * Create DTO from entity
     *
     * @param group {@link ApplicationGroup}
     */
    public ApplicationGroupInfoDTO(ApplicationGroup group) {
        this.id = group.getId();
        this.name = group.getName();
        this.description = group.getDescription();
        this.hideName = group.isHideName();
    }
}

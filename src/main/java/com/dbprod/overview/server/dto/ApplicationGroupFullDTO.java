package com.dbprod.overview.server.dto;

import com.dbprod.overview.server.models.ApplicationGroup;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * ApplicationGroupDTO POJO
 *
 * @author kapitoha
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationGroupFullDTO extends ApplicationGroupInfoDTO {
    private List<ApplicationDTO> applications;

    /**
     * Create DTO from entity
     *
     * @param group {@link ApplicationGroup}
     */
    public ApplicationGroupFullDTO(@NonNull ApplicationGroup group) {
        super(group);
        setApplications(group.getApplications().stream().map(ApplicationDTO::new).collect(Collectors.toList()));
    }
}

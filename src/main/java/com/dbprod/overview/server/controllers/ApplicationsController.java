package com.dbprod.overview.server.controllers;

import com.dbprod.overview.server.dto.ApplicationDTO;
import com.dbprod.overview.server.dto.ApplicationGroupFullDTO;
import com.dbprod.overview.server.dto.ApplicationGroupInfoDTO;
import com.dbprod.overview.server.models.ApplicationGroup;
import com.dbprod.overview.server.models.GalleryApplication;
import com.dbprod.overview.server.repositories.ApplicationGroupRepository;
import com.dbprod.overview.server.repositories.GalleryApplicationsRepository;
import com.dbprod.overview.server.services.PingService;
import com.dbprod.overview.server.utils.ApplicationUtils;
import com.dbprod.overview.server.utils.GroupUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * ApplicationsController POJO
 *
 * @author kapitoha
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/rest/applications/")
public class ApplicationsController {
    private final GalleryApplicationsRepository applicationsRepository;
    private final ApplicationGroupRepository groupRepository;
    private final PingService pingService;


    @Transactional
    @RequestMapping(value = "/groups", produces = "application/json")
    public List<ApplicationGroupFullDTO> getGroups() {
        List<ApplicationGroup> all = groupRepository.findAll();
        return all.stream().map(ApplicationGroupFullDTO::new).collect(Collectors.toList());
    }

    /**
     * Save {@link GalleryApplication} list
     *
     * @param dto {@link ApplicationDTO}
     * @return status
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    @PostMapping(value = "/application/save", produces = "application/json")
    public ApplicationDTO saveApplication(@RequestBody ApplicationDTO dto) {
        GalleryApplication application = Optional.ofNullable(dto.getId())
                .map(applicationsRepository::getOne)
                .orElse(new GalleryApplication());
        ApplicationUtils.resolveApplication(application, dto);
        return new ApplicationDTO(applicationsRepository.saveAndFlush(application));
    }

    /**
     * Check url is accessible
     *
     * @param url some url
     * @return json
     */
    @PostMapping(value = "/application/onlineStatus", produces = "application/json")
    public HashMap<String, Object> getApplicationOnlineStatus(@RequestParam(value = "url", required = false) String url) {
        final AtomicBoolean isOnline = new AtomicBoolean(false);
        Integer status = pingService.getStatus(url);
        isOnline.set(!List.of(-1, 404, 500).contains(status));
        return new HashMap<>() {{
            put("url", url);
            put("online", isOnline.get());
        }};
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @GetMapping(value = "/swapOrder")
    public ResponseEntity<?> setOrder(@RequestParam("application1") Integer applicationId1,
                                      @RequestParam("application2") Integer applicationId2) {
        GalleryApplication application1 = applicationsRepository.getOne(applicationId1);
        GalleryApplication application2 = applicationsRepository.getOne(applicationId2);
        if (Objects.equals(application1, application2)) {
            return ResponseEntity.noContent().build();
        } else {
            int orderIndex1 = application1.getOrderIndex();
            int orderIndex2 = application2.getOrderIndex();
            applicationsRepository.negateOrdering(List.of(applicationId1, applicationId2));
            application1.setOrderIndex(orderIndex2);
            application2.setOrderIndex(orderIndex1);
            applicationsRepository.saveAll(List.of(application1, application2));
        }
        return ResponseEntity.noContent().build();
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @GetMapping("/application/remove/{id}")
    public ResponseEntity<?> removeApplication(@PathVariable("id") Integer appId) {
        GalleryApplication application = applicationsRepository.getOne(appId);
        applicationsRepository.delete(application);
        return ResponseEntity.noContent().build();
    }

    @Transactional
    @PostMapping("/group/save")
    public ApplicationGroupFullDTO saveGroup(@RequestBody ApplicationGroupInfoDTO dto) {
        ApplicationGroup group = dto.getId() == null ? new ApplicationGroup() : groupRepository.getOne(dto.getId());
        GroupUtils.resolveGroup(group, dto);
        return new ApplicationGroupFullDTO(groupRepository.saveAndFlush(group));
    }

    @Transactional
    @GetMapping("/group/remove/{id}")
    public ResponseEntity<?> removeGroup(@PathVariable("id") Integer groupID,
                                         @RequestParam(value = "moveAppsTo", defaultValue = "0") Integer moveAppsToGroupId) {
        ApplicationGroup group = groupRepository.getOne(groupID);
        if (moveAppsToGroupId > 0 && !Objects.equals(groupID, moveAppsToGroupId)) {
            int maxOrderIndex = applicationsRepository.getMaxOrderIndex(moveAppsToGroupId);
            ApplicationGroup targetGroup = groupRepository.getOne(moveAppsToGroupId);
            Iterator<GalleryApplication> iterator = group.getApplications().iterator();
            // Place applications to the new group
            while (iterator.hasNext()) {
                GalleryApplication application = iterator.next();
                targetGroup.getApplications().add(application);
                application.setGroup(targetGroup);
                application.setOrderIndex(++maxOrderIndex);
                iterator.remove();
            }
            groupRepository.save(targetGroup);
        }
        groupRepository.delete(group);
        return ResponseEntity.noContent().build();
    }
}

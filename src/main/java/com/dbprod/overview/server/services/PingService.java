package com.dbprod.overview.server.services;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * Service responsible for URL online checking
 *
 * @author abilash
 */
@Log4j2
@Service
public class PingService {
    private static final Cache<String, Integer> URL_CACHE = Caffeine.newBuilder()
            .expireAfterWrite(2, TimeUnit.MINUTES).build();

    /**
     * Get URL status
     *
     * @param url some url
     * @return response http status or -1 if something went wrong.
     */
    public Integer getStatus(String url) {
        return URL_CACHE.get(url, s -> {
            try {
                HttpClient httpClient = HttpClient.newBuilder().followRedirects(HttpClient.Redirect.ALWAYS).build();
                HttpRequest request = HttpRequest.newBuilder(URI.create(url))
                        .timeout(Duration.of(4, TimeUnit.SECONDS.toChronoUnit())).build();
                HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                return response.statusCode();
            } catch (IOException | InterruptedException e) {
                return -1;
            }
        });
    }
}

package com.dbprod.overview.server.configurations;

/**
 * Constants POJO
 *
 * @author kapitoha
 */
public class Constants {
    public static final String DEFAULT_SCHEMA = "db_prod_overview";
}

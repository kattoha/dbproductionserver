package com.dbprod.overview.server.models;

import com.dbprod.overview.server.configurations.Constants;
import com.dbprod.overview.server.entityListeners.GalleryApplicationLifeCycleListener;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * GalleryApplication entity
 *
 * @author kapitoha
 */
@Entity
@Table(schema = Constants.DEFAULT_SCHEMA, name = "applications", uniqueConstraints = @UniqueConstraint(
        name = "order_index__uk", columnNames = {"application_group_id", "order_index"}))
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EntityListeners(GalleryApplicationLifeCycleListener.class)
public class GalleryApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    @NotEmpty
    @Column(nullable = false, length = 255)
    private String name;
    @Column(columnDefinition = "text")
    private String description;
    @Column(columnDefinition = "text")
    private String image;
    @Column(columnDefinition = "text")
    private String link;
    @NotNull
    @ToString.Exclude
    @JsonBackReference
    @ManyToOne(targetEntity = ApplicationGroup.class)
    @JoinColumn(name = "application_group_id", referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "application_group__fk"), nullable = false)
    private ApplicationGroup group;
    @Column(name = "order_index", nullable = false, unique = true)
    private int orderIndex;
}

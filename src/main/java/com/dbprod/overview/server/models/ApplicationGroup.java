package com.dbprod.overview.server.models;

import com.dbprod.overview.server.configurations.Constants;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * ApplicationGroup POJO
 *
 * @author kapitoha
 */
@Entity
@Table(schema = Constants.DEFAULT_SCHEMA, name = "gallery_groups")
@EqualsAndHashCode(of = "id")
@Getter
@Setter
@ToString
public class ApplicationGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name", nullable = false, columnDefinition = "text")
    private String name;
    @Column(columnDefinition = "text")
    private String description;
    @JsonManagedReference
    @OrderBy("orderIndex")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "group")
    private List<GalleryApplication> applications;
    /**
     * If true mark this group as noname
     */
    @Column(name = "hide_name", nullable = false, columnDefinition = "boolean default false")
    private boolean hideName;

    /**
     * Get applications list
     * @return list
     */
    public List<GalleryApplication> getApplications() {
        if (applications == null) {
            applications = new ArrayList<>();
        }
        return applications;
    }
}

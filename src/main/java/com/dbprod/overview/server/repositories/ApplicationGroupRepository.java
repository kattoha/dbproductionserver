package com.dbprod.overview.server.repositories;

import com.dbprod.overview.server.models.ApplicationGroup;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * GalleryGroupRepository Interface
 *
 * @author kapitoha
 */
public interface ApplicationGroupRepository extends JpaRepository<ApplicationGroup, Integer> {
}

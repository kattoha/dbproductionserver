package com.dbprod.overview.server.repositories;

import com.dbprod.overview.server.models.ApplicationGroup;
import com.dbprod.overview.server.models.GalleryApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * GalleryApplicationsRepository POJO
 *
 * @author kapitoha
 */
public interface GalleryApplicationsRepository extends JpaRepository<GalleryApplication, Integer> {
    @Query("SELECT COALESCE(MAX(e.orderIndex), 0)  FROM GalleryApplication e WHERE e.group.id = :group")
    int getMaxOrderIndex(@Param("group") int groupId);

    /**
     * Make order indexes negative for specified applications
     * @param applicationIDs list of IDs
     */
    @Transactional
    @Modifying
    @Query("UPDATE GalleryApplication a SET a.orderIndex = -a.orderIndex WHERE a.id IN (:ids)")
    void negateOrdering(@Param("ids") Iterable<Integer> applicationIDs);

    List<GalleryApplication> findAllByGroupOrderByOrderIndex(ApplicationGroup group);
}

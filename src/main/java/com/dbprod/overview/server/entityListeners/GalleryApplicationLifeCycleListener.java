package com.dbprod.overview.server.entityListeners;

import com.dbprod.overview.server.models.GalleryApplication;
import com.dbprod.overview.server.repositories.GalleryApplicationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import java.util.Optional;

/**
 * GalleryApplicationLifeCycleListener POJO
 *
 * @author kapitoha
 */
@Component
public class GalleryApplicationLifeCycleListener {
    private static GalleryApplicationsRepository applicationsRepository;

    @Autowired
    protected void init(GalleryApplicationsRepository applicationsRepository) {
        GalleryApplicationLifeCycleListener.applicationsRepository = applicationsRepository;
    }

    /**
     * Set default order index before persisting of the new application
     * @param application {@link GalleryApplication}
     */
    @PrePersist
    public void setDefaultOrder(GalleryApplication application) {
        if (application.getOrderIndex() <= 0) {
            int defaultIndex = applicationsRepository
                    .getMaxOrderIndex(application.getGroup().getId()) + 1;
            application.setOrderIndex(defaultIndex);
        }
    }
}
